//-----------------------------------------------------------------------
// terraform provider
// ----------------------------------------------------------------------
terraform {
  // -- use exist backet for save current state
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "s3tfb"
    region   = "ru-central1"
    key      = "k8sclustertest/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true

  }
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}
//-----------------------------------------------------------------------
// terraform data state:sa
// ----------------------------------------------------------------------
data "terraform_remote_state" "sa" {
  backend = "s3"
  config  = {
    endpoint = "storage.yandexcloud.net"

    //endpoints = {
    //  s3 = "https://storage.yandexcloud.net"
    //}
    bucket                      = "s3tfb"
    key                         = "backend/terraform.tfstate"
    region                      = "ru-central1"
    skip_region_validation      = true
    skip_credentials_validation = true

  }
}
#//-----------------------------------------------------------------------
#// terraform provider
#// ----------------------------------------------------------------------
#terraform {
#  // -- use exist backet for save current state
#  backend "s3" {
#    endpoints = {
#      s3 = "https://storage.yandexcloud.net"
#    }
#    bucket         = "s3tfb"
#    region         = "ru-central1"
#    key            = "k8scluster/terraform.tfstate"
#
#
#    skip_region_validation      = true
#    skip_credentials_validation = true
#    skip_requesting_account_id  = true # необходимая опция terraform для версии 1.6.1 и старше.
#    skip_s3_checksum            = true # необходимая опция при описании бэкенда для terraform версии 1.6.3 и старше.
#
#  }
#  required_providers {
#    yandex = {
#      source = "yandex-cloud/yandex"
#    }
#  }
#  required_version = ">= 0.13"
#}
#//-----------------------------------------------------------------------
#// terraform data state:sa
#// ----------------------------------------------------------------------
#data "terraform_remote_state" "sa" {
#  backend = "s3"
#  config  = {
#    endpoints = {
#      s3 = "https://storage.yandexcloud.net"
#    }
#    bucket         = "s3tfb"
#    region         = "ru-central1"
#    key            = "backend/terraform.tfstate"
#
#    #    access_key = "YCAJEcQ-6rEUCj69_kN5w5Prq"
#    #    secret_key = "YCNDVLLbmTP6RJDkJMIu0388C3lRr8V7EOCewgiq"
#    access_key = var.accesskey
#    secret_key = var.privatekey
#
#    skip_region_validation      = true
#    skip_credentials_validation = true
#    skip_requesting_account_id  = true # Необходимая опция при описании бэкенда для Terraform версии старше 1.6.1.
#  }
#}

# -- comment start segment --

//-----------------------------------------------------------------------
// vpc
// ----------------------------------------------------------------------
resource "yandex_vpc_network" "stockvpc" {
  name        = "stockvpc"
  description = "stockvpc for netologia"
  folder_id   = var.stockvars.folder_id
  labels      = {
    tf-label    = "vpc-netogia-stock"
    empty-label = ""
  }
}
//-----------------------------------------------------------------------
// public subnet for kubernetes cluster
// ----------------------------------------------------------------------
resource "yandex_vpc_subnet" "public-a" {
  name           = var.zonelistpublicsubnet.a.name
  zone           = var.zonelistpublicsubnet.a.zone
  folder_id      = var.stockvars.folder_id
  v4_cidr_blocks = [var.zonelistpublicsubnet.a.cidr]
  network_id     = yandex_vpc_network.stockvpc.id
}
resource "yandex_vpc_subnet" "public-b" {
  name           = var.zonelistpublicsubnet.b.name
  zone           = var.zonelistpublicsubnet.b.zone
  folder_id      = var.stockvars.folder_id
  v4_cidr_blocks = [var.zonelistpublicsubnet.b.cidr]
  network_id     = yandex_vpc_network.stockvpc.id
}
//-----------------------------------------------------------------------
// servers for kubernetes cluster
// ----------------------------------------------------------------------
//-----------------------------------------------------------------------
// master groups
// ----------------------------------------------------------------------
resource "yandex_compute_instance_group" "k8c-masters" {
  name                = "k8c-masters"
  folder_id = var.stockvars.folder_id
  service_account_id  = data.terraform_remote_state.sa.outputs.said
  deletion_protection = false
  labels              = {
    label1 = "masterk8s"
  }

  load_balancer {
    target_group_name = "k8c-masters"
  }

  instance_template {
    name = "master{instance.index}"

    resources {
      cores         = var.vmsetup.cores
      memory        = var.vmsetup.memory
      core_fraction = var.vmsetup.core_fraction
    }

    boot_disk {
      initialize_params {
        image_id = var.stockvars.image_id
        size     = var.vmsetup.disksize
        type     = var.vmsetup.disktype
      }
    }

    network_interface {
      #      network_id = yandex_vpc_network.stockvpc.id
      network_id = yandex_vpc_network.stockvpc.id
      subnet_ids = [
        yandex_vpc_subnet.public-a.id,
        yandex_vpc_subnet.public-b.id,
      ]
      nat = true
    }

    metadata = {
      ssh-keys = "ubuntu:${var.stockvars.sshkey}"
    }
    network_settings {
      type = "STANDARD"
    }
  }

  scale_policy {
    fixed_scale {
      size = var.stockvars.mastercount
    }
  }

  allocation_policy {
    zones = [
      var.stockvars.zonea,
      var.stockvars.zoneb,
    ]
  }

  deploy_policy {
    max_unavailable = var.stockvars.mastercount
    max_creating    = var.stockvars.mastercount
    max_expansion   = var.stockvars.mastercount
    max_deleting    = var.stockvars.mastercount
  }
}
//-----------------------------------------------------------------------
// worker group
// ----------------------------------------------------------------------
resource "yandex_compute_instance_group" "k8c-workers" {
  name               = "k8c-workers"
  service_account_id = data.terraform_remote_state.sa.outputs.said
  folder_id = var.stockvars.folder_id

  instance_template {
    name = "worker{instance.index}"

    resources {
      cores         = var.vmsetup.cores
      memory        = var.vmsetup.memory
      core_fraction = var.vmsetup.core_fraction
    }

    boot_disk {
      initialize_params {
        image_id = var.stockvars.image_id
        size     = var.vmsetup.disksize
        type     = var.vmsetup.disktype
      }
    }

    network_interface {
      network_id = yandex_vpc_network.stockvpc.id
      subnet_ids = [
        yandex_vpc_subnet.public-a.id,
        yandex_vpc_subnet.public-b.id,
      ]
      nat = true
    }

    metadata = {
      ssh-keys = "ubuntu:${var.stockvars.sshkey}"
    }
    network_settings {
      type = "STANDARD"
    }
  }

  scale_policy {
    fixed_scale {
      size = var.stockvars.workercount
    }
  }

  allocation_policy {
    zones = [
      var.stockvars.zonea,
      var.stockvars.zoneb,
    ]
  }

  deploy_policy {
    max_unavailable = var.stockvars.workercount
    max_creating    = var.stockvars.workercount
    max_expansion   = var.stockvars.workercount
    max_deleting    = var.stockvars.workercount
  }
}
//-----------------------------------------------------------------------
// ingress
// ----------------------------------------------------------------------
resource "yandex_compute_instance_group" "k8s-ingresses" {
  name               = "k8s-ingresses"
  service_account_id = data.terraform_remote_state.sa.outputs.said
  folder_id          = var.stockvars.folder_id
  load_balancer {
    target_group_name = "k8s-ingresses"
  }

  instance_template {
    name = "ingress-{instance.index}"
    resources {
      cores         = var.vmsetup.cores
      memory        = var.vmsetup.memory
      core_fraction = var.vmsetup.core_fraction
    }

    boot_disk {
      initialize_params {
        image_id = var.stockvars.image_id
        size     = var.vmsetup.disksize
        type     = var.vmsetup.disktype
      }
    }

    network_interface {
      network_id = yandex_vpc_network.stockvpc.id
      subnet_ids = [
        yandex_vpc_subnet.public-a.id,
        yandex_vpc_subnet.public-b.id,
      ]
      nat = true
    }

    metadata = {
      ssh-keys = "ubuntu:${var.stockvars.sshkey}"
    }
    network_settings {
      type = "STANDARD"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    zones = [
      "ru-central1-a",
      #      "ru-central1-b",
    ]
  }

  deploy_policy {
    max_unavailable = 1
    max_creating    = 1
    max_expansion   = 1
    max_deleting    = 1
  }
}

# Load balancer for ingresses
resource "yandex_lb_network_load_balancer" "k8s-load-balancer" {
  name       = "k8s-load-balancer"
  folder_id = var.stockvars.folder_id
  depends_on = [
    yandex_compute_instance_group.k8s-ingresses,
  ]

  listener {
    name = "my-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.k8s-ingresses.load_balancer.0.target_group_id
    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/healthz"
      }
    }
  }
}

# -- comment end segment --
