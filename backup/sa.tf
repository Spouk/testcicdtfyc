terraform {
  // -- use exist backet for save current state
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    
    bucket         = "s3tfb"
    region         = "ru-central1"
    key            = "satest/terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true

  }
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}
//-----------------------------------------------------------------------
// inject s3 backet root init
// ----------------------------------------------------------------------
data "terraform_remote_state" "s3tfb" {
  backend = "s3"
  config  = {
    endpoint = "storage.yandexcloud.net"
    
    //endpoints = {
    //  s3 = "https://storage.yandexcloud.net"
    //}
    bucket = "s3tfb"
    key    = "backend/terraform.tfstate"
    region = "ru-central1"
    skip_region_validation      = true
    skip_credentials_validation = true

  }
}

