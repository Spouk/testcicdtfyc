//-----------------------------------------------------------------------
// output result
// ----------------------------------------------------------------------
output "masters" {
  description = "ip masters"
  value       = <<EOF
  %{~ for x in yandex_compute_instance_group.k8c-masters.instances[*].network_interface.0 }
${x.nat_ip_address}:${x.ip_address}
  %{~ endfor }
  EOF
}
output "workers" {
  description = "ip workers"
  value       = <<EOF
  %{~ for x in yandex_compute_instance_group.k8c-workers.instances[*].network_interface.0 }
${x.nat_ip_address}:${x.ip_address}
  %{~ endfor }
  EOF
}
output "ingress" {
  description = "ip ingress"
  value       = <<EOF
  %{~ for x in yandex_compute_instance_group.k8s-ingresses.instances[*].network_interface.0 }
${x.nat_ip_address}:${x.ip_address}
  %{~ endfor }
  EOF
}
## - make host.ini for ansible -> kubespray
resource "local_file" "ansiblehostini_local" {
  content = templatefile("tmp.tftpl", {
    all = flatten([
      [for x in yandex_compute_instance_group.k8c-masters.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address, "master"]],
      [for x in yandex_compute_instance_group.k8c-workers.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address,"worker"]],
      [for x in yandex_compute_instance_group.k8s-ingresses.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address,"ingress"]]
    ])
    masters =  [for x in yandex_compute_instance_group.k8c-masters.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address]]
    workers = [for x in yandex_compute_instance_group.k8c-workers.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address]]
    ingress = [for x in yandex_compute_instance_group.k8s-ingresses.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address]]
  })
#  filename = "ansible/host.ini"
  filename = "host"
}

output"ansiblehostini2" {
  value  = templatefile("tmp.tftpl", {
    all = flatten([
      [for x in yandex_compute_instance_group.k8c-masters.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address, "master"]],
      [for x in yandex_compute_instance_group.k8c-workers.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address,"worker"]],
      [for x in yandex_compute_instance_group.k8s-ingresses.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address,"ingress"]]
    ])
    masters =  [for x in yandex_compute_instance_group.k8c-masters.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address]]
    workers = [for x in yandex_compute_instance_group.k8c-workers.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address]]
    ingress = [for x in yandex_compute_instance_group.k8s-ingresses.instances : [x.name,x.network_interface.0.ip_address, x.network_interface.0.nat_ip_address]]
  })
  #  filename = "ansible/host.ini"
}
output "masterip" {
  value = yandex_compute_instance_group.k8c-masters.instances[0].network_interface.0.nat_ip_address
}
output "lb" {
  value = yandex_compute_instance_group.k8s-ingresses.instances[0].network_interface.0.nat_ip_address
}
#resource "gitlab_repository_file" "masterip" {
#  branch    = "main"
#  content = yandex_compute_instance_group.k8c-masters.instances[0].network_interface.0.nat_ip_address
#  file_path = "ansible/masterip"
#  project   = 55697994
#}
## - make extern ip master k8
#resource "local_file" "masterip_local" {
#  filename = "ansible/masterip"
#  content = yandex_compute_instance_group.k8c-masters.instances[0].network_interface.0.nat_ip_address
#}
## - make extern ip load balance
#resource "local_file" "loadbalancerip_local" {
#  filename = "ansible/loadbalancerip"
#  content = yandex_compute_instance_group.k8s-ingresses.instances[0].network_interface.0.nat_ip_address
#}
