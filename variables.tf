//-----------------------------------------------------------------------
// global variables
// ----------------------------------------------------------------------
variable "stockvars" {
  type = object({
    folder_id    = string
    image_id     = string
    zone         = string
    kmastername  = string
    knodename    = string
    mastercount  = number
    workercount  = number
    zonea = string
    zoneb = string
    sshkey = string

  })
  default = {
    folder_id    = "b1g6jkfnul7b3vp4jegm"
    image_id     = "fd8vmcue7aajpmeo39kk"# Ubuntu 20.04 LTS
    zone         = "ru-central1-a"
    kmastername  = "master"
    knodename    = "worker"
    mastercount  = 1
    workercount  = 2 # for testig cicd pipeline tf with gitlab; yc have quota limit for counts VM
    zonea = "ru-central1-a"
    zoneb = "ru-central1-b"
    sshkey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCp88myjeoS35j0aJxbH8eOTlfHJNIAIFRshhJUdl+hAxslBtwblR57X0mwO1eWXx6MEAMTT918TnCutktzplWeSPQdYAmXLOfaeLTDvAytSRVr+eEwg2SfbQcKJh6sgUD5H9cDdaSMkLvCoiieI9dPz9hUCPz6gBiKoxg2seGn0kw0AYeiOqJecoqPeZ4I0qkt6zD+reqydJQy+x9KdtEoH5zizYV6LUmQNpEf+/cBQyMFn2pmO8rOUwYgrAW8vlZ/2q4D+8ejoXdRPUA8uj5B7B93cifhB75reiIM/+bAf+LQajR+GlddK7QA9ruLIjmS5/M3aaI3Bjq4Bc9cv6PAE5qq6QgtGo2MobUTgKZs6Ve24vSJk3eXzenaldAIvRcjbVmvagzL+a2NCcARZlSawoDoH3uzBYG2smaRi+CSpj8jMRddFlUhRCvQQdkJ+WPoGqTeT8vYNZ4+oi8FRhjqBdY+Bgj/RakJEx8C9zlgOmw9qnVUOWDRzvxC6GzuVV0= spouk@labnode"
  }
}

//-----------------------------------------------------------------------
// VM
// ----------------------------------------------------------------------
//instance:platform
variable "vmplatform" {
  type        = string
  default     = "standard-v1"
  description = "instance platform"
}
variable "vmsetup" {
  type    = map(any)
  default = {
    cores : 2,
    memory : 2,
    core_fraction : 5,
    disktype : "network-hdd",
    disksize : 20
  }
}
variable "metadata" {
  type    = map(any)
  default = {
    serial-port-enable : 1,
    ssh-keys : "ubuntu:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCp88myjeoS35j0aJxbH8eOTlfHJNIAIFRshhJUdl+hAxslBtwblR57X0mwO1eWXx6MEAMTT918TnCutktzplWeSPQdYAmXLOfaeLTDvAytSRVr+eEwg2SfbQcKJh6sgUD5H9cDdaSMkLvCoiieI9dPz9hUCPz6gBiKoxg2seGn0kw0AYeiOqJecoqPeZ4I0qkt6zD+reqydJQy+x9KdtEoH5zizYV6LUmQNpEf+/cBQyMFn2pmO8rOUwYgrAW8vlZ/2q4D+8ejoXdRPUA8uj5B7B93cifhB75reiIM/+bAf+LQajR+GlddK7QA9ruLIjmS5/M3aaI3Bjq4Bc9cv6PAE5qq6QgtGo2MobUTgKZs6Ve24vSJk3eXzenaldAIvRcjbVmvagzL+a2NCcARZlSawoDoH3uzBYG2smaRi+CSpj8jMRddFlUhRCvQQdkJ+WPoGqTeT8vYNZ4+oi8FRhjqBdY+Bgj/RakJEx8C9zlgOmw9qnVUOWDRzvxC6GzuVV0= spouk@labnode"
  }
}
variable "policy_nat" {
  type    = map(bool)
  default = {
    preemptible : true,
    nattrue : true,
    natfalse : false
  }
}
variable "image" {
  type    = string
  default = "ubuntu-2004-lts"
}
//-----------------------------------------------------------------------
// VPC
// ----------------------------------------------------------------------
variable "zonelistpublicsubnet" {
  description = "definitions objects subnet"
  type        = object({
    a = object({
      name = string
      zone = string
      cidr = string
    }),
    b = object({
      name = string
      zone = string
      cidr = string
    }),
  })
  default = {
    a = {
      name = "public-atest"
      zone = "ru-central1-a"
      cidr = "192.168.100.0/24"
    },
    b = {
      name = "public-btest"
      zone = "ru-central1-b"
      cidr = "192.168.110.0/24"
    },
  }
}
variable "zonelistsubnet" {
  description = "definitions objects subnet"
  type        = object({
    a = object({
      name = string
      zone = string
      cidr = string
    }),
    b = object({
      name = string
      zone = string
      cidr = string
    })
  })
  default = {
    a = {
      name = "private-atest"
      zone = "ru-central1-a"
      cidr = "10.10.0.0/16"
    },
    b = {
      name = "private-btest"
      zone = "ru-central1-b"
      cidr = "10.20.0.0/16"
    },

  }
}
